let should = require('should'),
    { PassphraseChecker } = require('../src/Day4');

describe('Passphrase 1', function () {
    let passphraseChecker = new PassphraseChecker();

    it('should return false if the file is empty', function () {
        return passphraseChecker.checkFile('', 'identical').catch(function (reason) {
            reason.should.be.exactly('File is not valid.');
        });
    });

    it('should return a number for valid input', function () {
        let file = 'tests/Day4input.txt';

        return passphraseChecker.checkFile(file, 'identical').then(function (result) {
            result.should.be.a.Number();
        });
    });

    it('should return 2 for the given example', function () {
        let file = 'tests/Day4input.txt';

        return passphraseChecker.checkFile(file, 'identical').then(function (result) {
            result.should.be.exactly(2);
        });
    });
});

describe('Passphrase 2', function () {
    let passphraseChecker = new PassphraseChecker();

    it('the validation function should return true for iiii oiii ooii oooi oooo', function () {
        passphraseChecker._validatePassphraseAnagram('iiii oiii ooii oooi oooo').should.be.true();
    });

    it('the validation function should return false for oiii ioii iioi iiio', function () {
        passphraseChecker._validatePassphraseAnagram('oiii ioii iioi iiio').should.be.false();
    });

    it('should return 3 for the given example', function () {
        let file = 'tests/Day4input2.txt';

        return passphraseChecker.checkFile(file, 'anagram').then(function (result) {
            result.should.be.exactly(3);
        });
    });
});