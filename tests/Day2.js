let should = require('should'),
    { Checksum } = require('../src/Day2');

describe('Checksum 1', function () {
    let checksum = new Checksum();

    it('should return false if the file is empty', function () {
        return checksum.checkFile('', 'subtraction').catch(function (reason) {
            reason.should.be.exactly('File is not valid.');
        });
    });

   it('should return a number for valid input', function () {
       let file = 'tests/Day2input.txt';

       return checksum.checkFile(file, 'subtraction').then(function (result) {
           result.should.be.a.Number();
       });
   });

   it('should return 18 for the sample file', function () {
       let file = 'tests/Day2input.txt';

       return checksum.checkFile(file, 'subtraction').then(function (result) {
           should(result).be.exactly(18);
       });
   });
});

describe('Checksum 2', function () {
    let checksum = new Checksum();

    it('should return false if the file is empty', function () {
        return checksum.checkFile('', 'division').catch(function (reason) {
            reason.should.be.exactly('File is not valid.');
        });
    });

    it('should return a number for valid input', function () {
        let file = 'tests/Day2input2.txt';

        return checksum.checkFile(file, 'division').then(function (result) {
            result.should.be.a.Number();
        });
    });

    it('should return 9 for the sample file', function () {
        let file = 'tests/Day2input2.txt';

        return checksum.checkFile(file, 'division').then(function (result) {
            should(result).be.exactly(9);
        });
    });
});