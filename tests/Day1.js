let should = require('should'),
    { ReverseCaptcha } = require('../src/Day1');

describe('Captcha 1', function () {
    let reverseCaptcha = new ReverseCaptcha();

    it('should return false if the input is not a string', function () {
        reverseCaptcha.nextDigit(123).should.be.false();
    });

    it('should return false if the string is empty', function () {
        reverseCaptcha.nextDigit('').should.be.false();
    });

    it('should return 3 for the input 1122', function () {
        reverseCaptcha.nextDigit('1122').should.be.exactly(3);
    });

    it('should return 4 for the input 1111', function () {
        reverseCaptcha.nextDigit('1111').should.be.exactly(4);
    });

    it('should return 0 for the input 1234', function () {
        reverseCaptcha.nextDigit('1234').should.be.exactly(0);
    });

    it('should return 9 for the input 91212129', function () {
        reverseCaptcha.nextDigit('91212129').should.be.exactly(9);
    });
});

describe('Captcha 2', function () {
    let reverseCaptcha = new ReverseCaptcha();

    it('should return false if the input is not a string', function () {
        reverseCaptcha.halfwayAround(123).should.be.false();
    });

    it('should return false if the string is empty', function () {
        reverseCaptcha.halfwayAround('').should.be.false();
    });

    it('should return 6 for the input 1212', function () {
        reverseCaptcha.halfwayAround('1212').should.be.exactly(6);
    });

    it('should return 0 for the input 1221', function () {
        reverseCaptcha.halfwayAround('1221').should.be.exactly(0);
    });

    it('should return 4 for the input 123425', function () {
        reverseCaptcha.halfwayAround('123425').should.be.exactly(4);
    });

    it('should return 12 for the input 123123', function () {
        reverseCaptcha.halfwayAround('123123').should.be.exactly(12);
    });

    it('should return 4 for the input 12131415', function () {
        reverseCaptcha.halfwayAround('12131415').should.be.exactly(4);
    });
});