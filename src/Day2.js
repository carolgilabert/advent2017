class Checksum {

    checkFile (file, method, callback) {
        var _this = this;

        return new Promise((resolve, reject) => {
            if (!file) {
                reject('File is not valid.');
            }

            this._readFile(file, function (result) {
                if (!result) {
                    reject('File could not be read.');
                }

                resolve(_this._processContent.call(_this, method, result));
            });
        });
    }

    _readFile(file, callback) {
        const fs = require('fs');

        fs.readFile(file, 'utf8', function (err, data) {
            if (err && typeof callback === 'function') {
                callback(false);
            }

            if (typeof callback === 'function') {
                callback(data);
            }
        });
    }

    _processContent(method, data) {
        let linesArray = data.split('\n');
        let sum = 0;

        for (let line of linesArray) {
            let numbersArray = line.match(/\S+/g);

            if (method === 'subtraction') {
                sum += this._processLineBySubtraction(numbersArray);
            } else if (method === 'division') {
                sum += this._processLineByDivision(numbersArray);
            }
        }

        return sum;
    }

    _processLineBySubtraction (numbersArray) {
        let min = false, max = false;

        for (let number of numbersArray) {
            number = Number.parseInt(number);

            if (!min || number < min) { min = Number.parseInt(number); }
            if (!max || number > max) { max = Number.parseInt(number); }
        }

        return (max-min);

    }

    _processLineByDivision (numbersArray) {
        let dividend = false, divisor = false;

        for (let i = 0; i < numbersArray.length; i++) {
            for (let j = 0; j < numbersArray.length; j++) {
                if (i === j) { continue; }

                let potentialDividend = numbersArray[i];
                let potentialDivisor = numbersArray[j];

                if (potentialDividend % potentialDivisor === 0) {
                    dividend = potentialDividend;
                    divisor = potentialDivisor;
                    break;
                }
            }
        }

        return (dividend/divisor);
    }
}

exports.Checksum = Checksum;