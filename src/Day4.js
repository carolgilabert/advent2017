class PassphraseChecker {
    checkFile (file, method) {
        var _this = this;

        return new Promise((resolve, reject) => {
            if (!file) {
                reject('File is not valid.');
            }

            this._readFile(file, function (result) {
                if (!result) {
                    reject('File could not be read.');
                }

                resolve(_this._processContent.call(_this, result, method));
            });
        });
    }

    _readFile(file, callback) {
        const fs = require('fs');

        fs.readFile(file, 'utf8', function (err, data) {
            if (err && typeof callback === 'function') {
                callback(false);
            }

            if (typeof callback === 'function') {
                callback(data);
            }
        });
    }

    _processContent(data, method) {
        let linesArray = data.split('\n');
        let sum = 0;

        for (let passphrase of linesArray) {
            if (method === 'identical') {
                sum += (this._validatePassphraseIdentical(passphrase)) ? 1 : 0;
            } else if (method === 'anagram') {
                sum += (this._validatePassphraseAnagram(passphrase)) ? 1 : 0;
            }
        }

        return sum;
    }

    _validatePassphraseIdentical (passphrase) {
        let wordsArray = passphrase.match(/\S+/g);

        while (wordsArray.length) {
            let word = wordsArray.splice(0, 1)[0];
            if (wordsArray.includes(word)) {
                return false;
            }
        }

        return true;
    }

    _validatePassphraseAnagram (passphrase) {
        let _this = this;
        let wordsArray = passphrase.match(/\S+/g);

        while (wordsArray.length) {
            let word = wordsArray.splice(0, 1)[0];
            let anagramExists = wordsArray.find(function (element) {
                return _this._areAnagrams.call(_this, word, element);
            });

            if (anagramExists) {
                return false;
            }
        }

        return true;
    }

    _areAnagrams (word1, word2) {
        if (word1.length !== word2.length) {
            return false;
        }

        let word1Sorted = word1.split('').sort().join('');
        let word2Sorted = word2.split('').sort().join('');

        for (let i = 0; i < word1.length; i++) {
            if (word1Sorted[i] !== word2Sorted[i]) {
                return false;
            }
        }

        return true;
    }
}

exports.PassphraseChecker = PassphraseChecker;