class ReverseCaptcha {
    nextDigit (input) {
        if (typeof input !== 'string' || !input) {
            return false;
        }

        return this._processCaptcha(input, 'next');
    }

    halfwayAround (input) {
        if (typeof input !== 'string' || !input) {
            return false;
        }

        return this._processCaptcha(input, 'halfway');
    }

    _processCaptcha (input, method) {
        let inputArray = input.split('');
        let sum = 0;

        for (let i = 0; i < inputArray.length; i++) {
            let jumpSize;

            if (method === 'next') {
                jumpSize = 1;
            } else if (method === 'halfway') {
                jumpSize = inputArray.length/2;
            }

            let nextIndex = (i+jumpSize) % inputArray.length;

            if (inputArray[i] === inputArray[nextIndex]) {
                sum += Number.parseInt(inputArray[i]);
            }
        }

        return sum;
    }

}

exports.ReverseCaptcha = ReverseCaptcha;